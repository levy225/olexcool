<?php

namespace App\Tests\Controller\Back;

use App\Controller\Back\NiveauController;
use App\Repository\NiveauRepository;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class NiveauControllerTest extends TestCase
{

    public function testIndex()
    {
        $niveauRepository = $this->getDoctrine()->getRepository('Niveau');
        $niveaux = $niveauRepository->findAll();
        return $this->respond($niveaux);


    }

    public function testEdit()
    {

    }

    public function testNew()
    {

    }

    public function testDelete()
    {

    }

    public function testShow()
    {

    }
}
