<?php


namespace App\Form\Front;

use App\Entity\Matiere;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class RechercheType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Recherche', TextType::class, [
                'required'   => false
            ])
            ->add('matiere', EntityType::class, [
                'class' => Matiere::class,
                'placeholder' => 'Matières',
                'required' => false,
                'choice_label' => 'libelle'
            ])
            ;
    }

}