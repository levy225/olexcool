<?php

namespace App\Form\Back;

use App\Entity\Matiere;
use App\Entity\Niveau;
use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('email')
            ->add('plainPassword', RepeatedType::class, [
                'type' =>   PasswordType::class,
                'invalid_message' => 'The password fiels must match',
                'options' => ['attr' => ['class' => 'password-field']],
                'required' => true,
                'first_options'  => ['label' => 'Password'],
                'second_options' => ['label' => 'Repeat Password']
            ] )
            ->add('nom')
            ->add('prenom')
            ->add('sexe')
            ->add('dateNaissance', DateType::class, [
                'widget' => 'single_text'
            ])
            ->add('telephone')
            ->add('adresse')
            ->add('isVerified')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
