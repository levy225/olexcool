<?php

namespace App\Form\Back;

use App\Entity\Demande;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class DemandeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date_demande', DateTimeType::class, [
                'widget' => 'single_text'
            ])
            ->add('commentaire', TextType::class)
            ->add('date_cours', DateTimeType::class, [
                'widget' => 'single_text'
            ])
            ->add('type_demande', ChoiceType::class, [
                'choices' => Demande::TYPE ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Demande',
        ]);
    }
}
