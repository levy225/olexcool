<?php

namespace App\Repository;

use App\Entity\Demande;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Demande|null find($id, $lockMode = null, $lockVersion = null)
 * @method Demande|null findOneBy(array $criteria, array $orderBy = null)
 * @method Demande[]    findAll()
 * @method Demande[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DemandeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Demande::class);
    }


    public function findAllfor($value)
    {

        return $this->createQueryBuilder('d')
            ->andWhere('d.enseignant = :val')
            ->setParameter('val', $value)
            ->orderBy('d.date_demande', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }
    /* Find Demande by Eleve*/
    public function findByEleve($value)
    {

        return $this->createQueryBuilder('d')
            ->andWhere('d.eleve = :val')
            ->andWhere('d.enseignant is not NULL')
            ->setParameter('val', $value)
            ->orderBy('d.date_demande', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    /* Find Demande by Enseignant*/
    public function findByEnseignant($value)
    {

        return $this->createQueryBuilder('d')
            ->andWhere('d.enseignant = :val')
            ->andWhere('d.enseignant is not NULL')
            ->setParameter('val', $value)
            ->orderBy('d.date_demande', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findByEnseignantNotAccepted($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.enseignant = :val')
            ->andWhere('d.enseignant is not NULL')
            ->andWhere('d.is_accepted = false')
            ->setParameter('val', $value)
            ->orderBy('d.date_demande', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    /* Find Demande by Enseignant*/
    public function findDemandeWithoutEnseignant() {
        return $this->createQueryBuilder('d')
            ->andWhere('d.enseignant is NULL')
            ->orderBy('d.date_demande', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findMatiereByEnseignant(){
        return $this->createQueryBuilder('d')
            ->andWhere('d.enseignant is NOT NULL')
            ->orderBy('d.matieres', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }




    // /**
    //  * @return Demande[] Returns an array of Demande objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Demande
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
