<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @return User[] Returns an array of User objects
     */
    public function findByRole($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.roles like :val')
            ->setParameter('val', $value[0])
            ->orderBy('u.id', 'ASC')
            ->getQuery()->getResult()
            ;
    }

    public function findAllTeachersQuery()
    {
        $role = 'ROLE_ENSEIGNANT';

        return $this->createQueryBuilder('u')
            ->andWhere('json_role(u.roles) LIKE :role' )
            ->leftJoin('u.matieres', 'm')
            ->setParameter('role', '%' . $role . '%' )
            ->getQuery()
            ->getResult()
            ;
    }

    public function search($search, $matiere)
    {
        $role = 'ROLE_ENSEIGNANT';

        return $this->createQueryBuilder('u')
            ->andWhere('LOWER(u.nom) like :nom or LOWER(u.prenom) like :prenom')
            ->andWhere('json_role(u.roles) LIKE :role')
            ->andWhere('m.id = :matiere')
            ->leftJoin('u.matieres', 'm')
            ->setParameter('nom', '%'.$search.'%')
            ->setParameter('prenom', '%'.$search.'%')
            ->setParameter('role', '%' . $role . '%')
            ->setParameter('matiere', $matiere)
            ->getQuery()
            ->getResult()
            ;
    }

    public function searchAllMatiere($search)
    {
        $role = 'ROLE_ENSEIGNANT';

        return $this->createQueryBuilder('u')
            ->andWhere('LOWER(u.nom) like :nom or LOWER(u.prenom) like :prenom')
            ->andWhere('json_role(u.roles) LIKE :role')
            ->leftJoin('u.matieres', 'm')
            ->setParameter('nom', '%'.$search.'%')
            ->setParameter('prenom', '%'.$search.'%')
            ->setParameter('role', '%' . $role . '%')
            ->getQuery()
            ->getResult()
            ;
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
