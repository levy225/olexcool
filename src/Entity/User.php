<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="user_account")
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface
{

    const SEXE = [
        'Homme' => 'm',
        'Femme' => 'f'
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @var string plainPassword
     */
    private $plainPassword;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Choice(choices=User::SEXE)
     * @Assert\Expression("this.getSexe() != null", message="Sexe invalid")
     */
    private $sexe;

    /**
     * @ORM\Column(type="date")
     * @Assert\Expression("this.getDateNaissance() < this.getCurrentDate()",
     *     message="We don't accepte futur babies for the moment")
     */
    private $dateNaissance;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $telephone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresse;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isVerified;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=225, unique=true)
     *
     * @Gedmo\Slug(fields={"nom","prenom"})
     */
    private $slug;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $confirmationToken;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Message", mappedBy="transmitter")
     */
    private $sendMessages;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Message", mappedBy="receiver")
     */
    private $receiveMessages;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Demande", mappedBy="eleve")
     */
    private $demandeSend;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Demande", mappedBy="enseignant")
     */
    private $demandeReceive;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Matiere", inversedBy="users")
     */
    private $matieres;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $credits;

    /**
     * @ORM\OneToMany(targetEntity=Paiement::class, mappedBy="person")
     */
    private $paiements;


    public function __construct()
    {
        $this->roles = ['ROLE_USER'];
        $this->credits = 0;
        $this->isVerified = false;
        $this->demandes = new ArrayCollection();
        $this->sendMessages = new ArrayCollection();
        $this->receiveMessages = new ArrayCollection();
        $this->demandeSend = new ArrayCollection();
        $this->demandeReceive = new ArrayCollection();
        $this->matieres = new ArrayCollection();
        $this->paiements = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $temp = array_merge($this->roles, $roles);
        $this->roles = null;
        $this->roles = $temp;

        return $this;
    }


    public function getStringRoles(): string
    {
        return json_encode($this->getRoles());

    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getSexe(): ?string
    {
        return $this->sexe;
    }

    public function setSexe(string $sexe): self
    {
        $this->sexe = $sexe;

        return $this;
    }

    public function getDateNaissance(): ?\DateTimeInterface
    {
        return $this->dateNaissance;
    }

    public function setDateNaissance(\DateTimeInterface $dateNaissance): self
    {
        $this->dateNaissance = $dateNaissance;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getIsVerified(): ?bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getConfirmationToken(): ?string
    {
        return $this->confirmationToken;
    }

    public function setConfirmationToken(?string $confirmationToken): self
    {
        $this->confirmationToken = $confirmationToken;

        return $this;
    }

    /**
     * @return string
     */
    public function getPlainPassword():? string
    {
        return $this->plainPassword;
    }

    /**
     * @param string $plainPassword
     * @return User
     */
    public function setPlainPassword(string $plainPassword):? User
    {
        $this->plainPassword = $plainPassword;
        return $this;
    }

    /**
     * @return Collection|Message[]
     */
    public function getSendMessages(): Collection
    {
        return $this->sendMessages;
    }

    public function addSendMessage(Message $sendMessage): self
    {
        if (!$this->sendMessages->contains($sendMessage)) {
            $this->sendMessages[] = $sendMessage;
            $sendMessage->setTransmitter($this);
        }

        return $this;
    }

    public function removeSendMessage(Message $sendMessage): self
    {
        if ($this->sendMessages->contains($sendMessage)) {
            $this->sendMessages->removeElement($sendMessage);
            // set the owning side to null (unless already changed)
            if ($sendMessage->getTransmitter() === $this) {
                $sendMessage->setTransmitter(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Message[]
     */
    public function getReceiveMessages(): Collection
    {
        return $this->receiveMessages;
    }

    public function addReceiveMessage(Message $receiveMessage): self
    {
        if (!$this->receiveMessages->contains($receiveMessage)) {
            $this->receiveMessages[] = $receiveMessage;
            $receiveMessage->setReceiver($this);
        }

        return $this;
    }

    public function removeReceiveMessage(Message $receiveMessage): self
    {
        if ($this->receiveMessages->contains($receiveMessage)) {
            $this->receiveMessages->removeElement($receiveMessage);
            // set the owning side to null (unless already changed)
            if ($receiveMessage->getReceiver() === $this) {
                $receiveMessage->setReceiver(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Demande[]
     */
    public function getDemandeSend(): Collection
    {
        return $this->demandeSend;
    }

    public function addDemandeSend(Demande $demandeSend): self
    {
        if (!$this->demandeSend->contains($demandeSend)) {
            $this->demandeSend[] = $demandeSend;
            $demandeSend->setEleve($this);
        }

        return $this;
    }

    public function removeDemandeSend(Demande $demandeSend): self
    {
        if ($this->demandeSend->contains($demandeSend)) {
            $this->demandeSend->removeElement($demandeSend);
            // set the owning side to null (unless already changed)
            if ($demandeSend->getEleve() === $this) {
                $demandeSend->setEleve(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Demande[]
     */
    public function getDemandeReceive(): Collection
    {
        return $this->demandeReceive;
    }

    public function addDemandeReceive(Demande $demandeReceive): self
    {
        if (!$this->demandeReceive->contains($demandeReceive)) {
            $this->demandeReceive[] = $demandeReceive;
            $demandeReceive->setEnseignant($this);
        }

        return $this;
    }

    public function removeDemandeReceive(Demande $demandeReceive): self
    {
        if ($this->demandeReceive->contains($demandeReceive)) {
            $this->demandeReceive->removeElement($demandeReceive);
            // set the owning side to null (unless already changed)
            if ($demandeReceive->getEnseignant() === $this) {
                $demandeReceive->setEnseignant(null);
            }
        }

        return $this;
    }


    public function getSlug(): string
    {
        return $this->slug;
    }


    public function setSlug($slug): string
    {
        $this->slug = $slug;
    }

    /**
     * @return Collection|Matiere[]
     */
    public function getMatieres(): Collection
    {
        return $this->matieres;
    }

    public function addMatiere(Matiere $matiere): self
    {
        if (!$this->matieres->contains($matiere)) {
            $this->matieres[] = $matiere;
        }

        return $this;
    }

    public function removeMatiere(Matiere $matiere): self
    {
        if ($this->matieres->contains($matiere)) {
            $this->matieres->removeElement($matiere);
        }

        return $this;
    }

    public function getCredits(): ?int
    {
        return $this->credits;
    }

    public function setCredits(?int $credits): self
    {
        $this->credits = $credits;

        return $this;
    }

    /**
     * @return Collection|Paiement[]
     */
    public function getPaiements(): Collection
    {
        return $this->paiements;
    }

    public function addPaiement(Paiement $paiement): self
    {
        if (!$this->paiements->contains($paiement)) {
            $this->paiements[] = $paiement;
            $paiement->setPerson($this);
        }

        return $this;
    }

    public function removePaiement(Paiement $paiement): self
    {
        if ($this->paiements->contains($paiement)) {
            $this->paiements->removeElement($paiement);
            // set the owning side to null (unless already changed)
            if ($paiement->getPerson() === $this) {
                $paiement->setPerson(null);
            }
        }
        return $this;
    }

    public function getCurrentDate(){
        return new \DateTimeImmutable();
    }
}
