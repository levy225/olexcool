<?php

namespace App\Controller\Front;

use App\Entity\Demande;
use App\Entity\User;
use App\Entity\Matiere;
use App\Form\Front\DemandeType;
use App\Repository\DemandeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Service\MailerService;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DemandeController
 * @package App\Controller\Front
 * @Route("/request")
 */
class DemandeController extends AbstractController
{

    /**
     * @Route("/", name="request_index", methods={"GET"})
     * @param DemandeRepository $demandeRepository
     * @return Response
     */
    public function index(DemandeRepository $demandeRepository): Response
    {
        $user = $this->getUser();
        if($user->getRoles() == ["ROLE_USER","ROLE_ELEVE"]){
            $demandes = $demandeRepository->findByEleve($user->getId());
        } elseif($user->getRoles() == ["ROLE_USER","ROLE_ENSEIGNANT"]){
            $demandes = $demandeRepository->findByEnseignant($user->getId());
        }

        return $this->render('front/demande/index.html.twig', [
            'demandes' => $demandes,
            'user' => $user
        ]);
    }

    /**
     * @Route("/list/", name = "request_list", methods={"GET", "POST"})
     */
    public function list(DemandeRepository $demandeRepository): Response {
        $demandes = $demandeRepository->findDemandeWithoutEnseignant();
        $myDemandes = $demandeRepository->findByEnseignantNotAccepted($this->getUser()->getId());

        return $this->render('front/demande/list.html.twig', [
            'myDemandes' => $myDemandes,
            'demandes' => $demandes,
        ]);
    }


    /**
     * @Route("/new", name="request_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $demande = new Demande();
        $form = $this->createForm(DemandeType::class, $demande);
        $form->handleRequest($request);

        $demande->setDateDemande(new \DateTime());
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($demande);
            $entityManager->flush();

            return $this->redirectToRoute('front_default_index');
        }

        return $this->render('front/demande/new.html.twig', [
            'demande' => $demande,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/newall", name="request_newall", methods={"GET","POST"})
     */
    public function newall(Request $request): Response
    {
        $eleve = $this->getUser();

        $demande = new Demande();
        $form = $this->createForm(DemandeType::class, $demande);
        $form->handleRequest($request);

        $demande->setEleve($eleve);
        $demande->setDateDemande(new \DateTime());
        if ($form->isSubmitted() && $form->isValid()) {
            $matiere= $this->getDoctrine()->getRepository(Matiere::class)->find($_POST['matiere']);
            $demande->setMatiere($matiere);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($demande);
            $entityManager->flush();

            return $this->redirectToRoute('front_default_index');
        }

        return $this->render('front/demande/newall.html.twig', [
            'demande' => $demande,
            'eleve' => $eleve,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name = "teachers_request", methods={"GET", "POST"})
     */

    public function teachersRequest(Request $request, MailerInterface $mailer, MailerService $mailerService, $id ): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $enseignant = $entityManager->getRepository(User::class)->find($id);

        $eleve = $this->getUser();

        $demande = new Demande();
        $form = $this->createForm(DemandeType::class, $demande);
        $form->handleRequest($request);


        $demande->setEnseignant($enseignant);
        $demande->setEleve($eleve);
        $demande->setTarif((int)$request->request->get('tarif'));
        $demande->setDateDemande(new \DateTime());
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($demande);
            $entityManager->flush();

            $this->addFlash('success', 'Votre demande a bien été envoyée !');


            //Crediter les comptes
            $eleve->setCredits($eleve->getCredits() - 10);
            $enseignant->setCredits(10);
            $entityManager->persist($eleve);
            $entityManager->persist($enseignant);
            $entityManager->flush();




            $to = $enseignant->getEmail();
            $subject = "Demande d'aide ! ";
            $fullName = $eleve->getPrenom()." ".$eleve->getNom();
            $body_text = "L'élève {$fullName}! \n a besoin d'aide ! :\nhttp://localhost:8080/fr/request/list";

            $mailerService->sendEmail($mailer, $to, $subject, $body_text);

            return $this->redirectToRoute('front_enseignants_show');
        }

        return $this->render('front/demande/new.html.twig', [
            'demande' => $demande,
            'enseignant' => $enseignant,
            'form' => $form->createView(),
        ]);
    }





}
