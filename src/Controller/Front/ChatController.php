<?php

namespace App\Controller\Front;

use App\Entity\Demande;
use App\Entity\Message;
use App\Entity\User;
use App\Form\Back\DemandeType;
use App\Form\Front\MessageType;
use App\Repository\DemandeRepository;
use App\Repository\MessageRepository;
use App\Repository\UserRepository;
use App\Service\MercureCookieGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mercure\Publisher;
use Symfony\Component\Mercure\PublisherInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class ChatController extends AbstractController
{
    /**
     * @Route("/chat/", name="chat", methods={"GET"})
     */
    public function showMessage(MessageRepository $messageRepository): Response
    {

        $user = $this->getUser();
        $messages = $messageRepository->findAllfor($user->getId());
        return $this->render('front/chat/show.html.twig', [
            'messages' => $messages,
        ]);
    }

    /**
     * @Route("/chat/{demande_id}", name="chat_demande", methods={"GET", "POST"})
     * @param MessageRepository $messageRepository
     * @return Response
     */
    public function showMessageDemande(Request $request, $demande_id, MessageRepository $messageRepository, DemandeRepository $demandeRepository, MercureCookieGenerator $cookieGenerator): Response
    {

        $user = $this->getUser();

        $messages = [];

        $demande = $demandeRepository->find($demande_id);
        foreach ($messageRepository->findbyDemande(intval($demande_id)) as $value){
            array_push($messages, $value);
        }

        $message = new Message();
        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);


        if ($demande->getEleve() !== null && $demande->getEnseignant() == null) {
            $demande->setEnseignant($user);
            $receiver = $demande->getEleve();
        } elseif($demande->getEleve() == null && $demande->getEnseignant() !== null) {
            $demande->setEleve($user);
            $receiver = $demande->getEnseignant();
        } elseif ($demande->getEleve()->getId() !== $user->getId()) {
            $receiver = $demande->getEleve();
        } elseif ($demande->getEnseignant()->getId() !== $user->getId()) {
            $receiver = $demande->getEnseignant();
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($demande);
        $entityManager->flush();


        $response = $this->render('front/chat/show.html.twig', [
            'messages' => $messages,
            'demande' => $demande,
            'date_demande' => $demande->getDateDemande(),
            'receiver' => $receiver,
            'transmitter' => $user,
            'form' => $form->createView()
        ]);
        $response->headers->set('set-cookie', $cookieGenerator->generate($this->getUser()));
        return $response;
    }

    /**
     * @Route("/chat/new/{demande_id}/{transmitter_id}/{receiver_id}", name="chat_new", methods={"POST"})
     * @param Request $request
     * @param $demande_id
     * @param $receiver_id
     * @param $transmitter_id
     * @param MessageRepository $messageRepository
     * @param DemandeRepository $demandeRepository
     * @param UserRepository $userRepository
     * @param MessageBusInterface $bus
     * @return Response
     * @throws \Exception
     */
    public function sendMessage(Request $request, $demande_id, $receiver_id, $transmitter_id, MessageRepository $messageRepository, DemandeRepository $demandeRepository, UserRepository $userRepository, MessageBusInterface $bus): Response
    {
        $message = new Message();

        $target = ["http://chat.olexcool/user/{$receiver_id}"];
        $updateMercure = new Update('http://chat.olexcool/message/', $request->request->get('content'), $target);
        $bus->dispatch($updateMercure);

        $transmitter = $userRepository->find($transmitter_id);
        $receiver = $userRepository->find($receiver_id);
        $demande = $demandeRepository->find($demande_id);

        $date = new \DateTime('', new \DateTimeZone('Europe/Paris'));

        $message
            ->setContent($request->request->get('content'))
            ->setDate($date)
            ->setTransmitter($transmitter)
            ->setReceiver($receiver)
            ->setDemande($demande)
        ;


        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($message);
        $entityManager->flush();

        return new Response(
            true,
            Response::HTTP_OK
        );
    }
}
