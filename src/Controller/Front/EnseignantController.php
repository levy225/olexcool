<?php

namespace App\Controller\Front;

use App\Entity\Demande;
use App\Form\Front\DemandeType;
use App\Form\Front\MessageType;
use App\Form\Front\RechercheType;
use App\Repository\UserRepository;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

// Include paginator interface
use Knp\Component\Pager\PaginatorInterface;

class EnseignantController extends AbstractController
{
    /**
     * @Route("/teachers/", name="enseignants_show", methods={"GET"})
     * @param UserRepository $userRepository
     * @return Response
     */
    public function teachers(Request $request, UserRepository $userRepository, PaginatorInterface $paginator): Response
    {

        $form = $this->createForm(RechercheType::class);

        $teachers = $userRepository->findAllTeachersQuery();


        $pagination = $paginator->paginate(
            $teachers, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            12  /*limit per page*/
        );


        // parameters to template
        return $this->render('front/enseignant/show.html.twig', [
            'pagination' => $pagination,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/teachers/search", name="enseignant_search", methods={"POST","GET"})
     * @return Response
     */
    public function search(Request $request,PaginatorInterface $paginator, UserRepository $userRepository): Response
    {
        $form = $this->createForm(RechercheType::class);
        $form->handleRequest($request);


//        if ($form->isSubmitted() && $form->isValid()){

            $recherche = $form->getData('recherche');

            $value = strtolower($recherche['Recherche']);

            if (isset($recherche['matiere'])) {
                $teachers = $userRepository->search($value, $recherche['matiere']);
            } else {
                $teachers = $userRepository->searchAllMatiere($value);
            }

            $pagination = $paginator->paginate(
                $teachers, /* query NOT result */
                $request->query->getInt('page', 1), /*page number*/
                12  /*limit per page*/
            );


            // parameters to template
            return $this->render('front/enseignant/show.html.twig', [
                'pagination' => $pagination,
                'form' => $form->createView()

            ]);

        }

//    }
}
