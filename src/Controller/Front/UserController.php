<?php

namespace App\Controller\Front;

use App\Entity\Paiement;
use App\Entity\User;
use App\Form\Front\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * Class UserController
 * @package App\Controller\Front
 * @Route("/user")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/", name="user", methods={"GET", "POST"})
     */
    public function profile(Request $request, Security $security): Response
    {
        $user = $security->getUser();

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash('sucess', 'Votre profil a été mis à jour');
            return $this->redirectToRoute('front_user');
        }

        return $this->render('front/user/show.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/paiment", name="user_paiement", methods={"GET"})
     */
    public function paiement(Request $request): Response
    {
        $amount = (int)$request->query->get('credits');

        \Stripe\Stripe::setApiKey('sk_test_bSMrC7UPuz4FyUsyfuMPiCWI00sTeYI6ZO');
        $intent = \Stripe\PaymentIntent::create([
           'amount' => $amount*100,
           'currency' => 'eur',
            // Verify your integration in this guide by including this parameter
            'metadata' => ['integration_check' => 'accept_a_payment'],
        ]);

        return $this->render('front/user/paiement.html.twig', [
            'intent' => $intent,
            'amount' => $amount,
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/checkout", name="user_checkout", methods={"POST"})
     */
    public function checkout(Request $request): Response
    {
        $statut = (bool)$request->request->get('statut');


        if($statut) {
            $amount = (int)$request->request->get('amount')/100;

            $user = $this->getUser();
            $credits = $user->getCredits() + $amount;
            $user->setCredits($credits);

            $paiement = new Paiement();
            $paiement->setMontant($amount);
            $paiement->setDate(new \DateTime());
            $paiement->setPerson($user);
            $paiement->setTransactionType("Achat");

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->persist($paiement);
            $entityManager->flush();
        }

        return $this->redirectToRoute('front_user');

    }

}
