<?php

namespace App\Controller\Front;

use App\Entity\Matiere;
use App\Form\Front\MatiereType;
use App\Repository\MatiereRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MatiereController extends AbstractController
{
    /**
     * @Route("/addmatiere", name="matiere_index", methods={"GET", "POST"})
     */
    public function index(MatiereRepository $matiereRepository, Request $request): Response
    {
        $form = $this->createForm(MatiereType::class);
        $user = $this->getUser();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $matieres = $form->getData('matiere');
            foreach ($matieres as $matiere){
                $user->addMatiere($matiere);
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('front_default_index');
        }
        return $this->render('front/matiere/index.html.twig', [
            'form' => $form->createView(),
        ]);

    }
}