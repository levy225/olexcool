<?php

namespace App\Controller\Front;

use App\Entity\User;
use App\Form\ForgottenPasswordType;
use App\Form\RegistrationFormType;
use App\Form\ResetPasswordType;
use App\Service\MailerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="register", methods={"GET", "POST"})
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     * @throws \Exception
     */
    public function register(MailerInterface $mailer, MailerService $mailerService, Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setConfirmationToken($mailerService->generateToken());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $to = $user->getEmail();
            $subject = "Welcome to Olexcool";
            $mail_template = 'email/welcome.html.twig';
            $mailerService->sendMail($mailer, $to, $subject, $mail_template, $user);
            $this->addFlash('success','Veuillez confirmer votre adresse mail en cliquant sur le lien envoyé à votre boîte de réception');

            return $this->redirectToRoute('email');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }


    /**
     * @Route("/confirm/{token}", name="confirm_account")
     * @param $token
     * @return Response
     */
    public function confirmAccount($token): Response
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->findOneBy(['confirmationToken' => $token]);
        if($user) {
            $user->setConfirmationToken(null);
            $user->setIsVerified(true);
            $em->persist($user);
            $em->flush();
            $this->addFlash('success', 'Votre compte a été activé avec success');
            return $this->redirectToRoute('app_login');
        } else {
            return $this->redirectToRoute('app_login');
        }
    }


    /**
     * @Route("/forget-password", name="forgotten_password")
     * @param Request $request
     * @param MailerService $mailerService
     * @param MailerInterface $mailer
     * @return Response
     * @throws \Exception
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function forgottenPassword(Request $request, MailerService $mailerService, MailerInterface $mailer): Response
    {
        $form = $this->createForm(ForgottenPasswordType::class, ['email' => $request->get('email')]);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $data = $form->getData();
            $user = $this->getDoctrine()
                ->getRepository(User::class)
                ->findOneBy(['email' => $data['email']]);
            if($user === null){
                $this->addFlash(    'error', 'utilisateur non trouvé');
                return $this->redirectToRoute('register');
            }
            $user->setConfirmationToken($mailerService->generateToken());
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $to = $user->getEmail();
            $subject = "Mot de passe oublié";
            $mail_template = 'email/forgotten_password.html.twig';
            $mailerService->sendMail($mailer, $to, $subject, $mail_template, $user);
            $this->addFlash('success','Un mail avec un lien de réinitialisation vous a été envoyé');
            return $this->redirectToRoute('app_login');
        }
        return $this->render('registration/forgotten_password.html.twig',[
            'forgottenPassword' => $form->createView()
        ]);
    }

    /**
     * @Route("/reset-password/{token}", name="reset_password", methods={"GET", "POST"})
     * @param Request $request
     * @param $token
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function resetPassword(Request $request, MailerService $mailerService, UserPasswordEncoderInterface $passwordEncoder, $token, MailerInterface $mailer):Response
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->findOneBy(['confirmationToken' => $token]);
        $form = $this->createForm(ResetPasswordType::class);
        $form->handleRequest($request);
//        dump($form);

        if($user) {
            if ($form->isSubmitted() && $form->isValid()) {
                $user->setConfirmationToken(null);
                $user->setPassword(
                    $passwordEncoder->encodePassword(
                        $user,
                        $form->get('plainPassword')->getData()
                    )
                );
                $user->setIsVerified(true);
                $em->persist($user);
                $em->flush();
                $to = $user->getEmail();
                $subject = "Réinitialisation de votre mot de passe";
                $mail_template = 'email/reset_password.html.twig';
                $mailerService->sendMail($mailer, $to, $subject, $mail_template, $user);

                return $this->redirectToRoute('app_login');
            }
        }else {
            $this->addFlash('not-user-exist', 'utilisateur non trouvé');
            return $this->redirectToRoute('app_login');
        }
        return $this->render('registration/reset_password.html.twig', [
            'resetPassword' => $form->createView()
        ]);
    }

}
