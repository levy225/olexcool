<?php


namespace App\DataFixtures;


use App\Entity\Matiere;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture implements DependentFixtureInterface
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }


    public function load(ObjectManager $manager)
    {

        $faker = \Faker\Factory::create();
        $matiere = $manager->getRepository(Matiere::class)->findAll();

        for ($i = 0; $i < 50; $i++) {
            $user = (new User())
                ->setNom($faker->lastName)
                ->setPrenom($faker->firstName)
                ->setEmail($faker->email)
                ->setPassword('string')
                ->setSexe($faker->randomElement(User::SEXE))
                ->setDateNaissance($faker->dateTime)
                ->setTelephone($faker->phoneNumber)
                ->setAdresse($faker->address)
                ->setDescription($faker->sentence(20))
                ->setIsVerified(false)
                ->setConfirmationToken(rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '='))
                ->addMatiere($faker->randomElement($matiere))
                ->setCredits(0)
            ;
            if( $i%2  == 0){
                $user->setRoles(["ROLE_ELEVE"]);
            }
            else {
                $user->setRoles(["ROLE_ENSEIGNANT"]);
            }

            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                'string'
            ));

            $manager->persist($user);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            MatiereFixtures::class,
        );
    }
}