<?php


namespace App\DataFixtures;


use App\Entity\Demande;
use App\Entity\Message;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class MessagerieFIxtures extends Fixture implements DependentFixtureInterface
{

    public function load(ObjectManager $manager){

        $faker = \Faker\Factory::create();

        $user = $manager->getRepository(User::class)->findAll();
        $demandes = $manager->getRepository(Demande::class)->findAll();

        for ($i = 0; $i < 100; $i++){

            $demande = $faker->randomElement($demandes);
            $user = [$demande->getEleve(), $demande->getEnseignant()];

            $transmitter = $faker->randomElement($user);
            if ($transmitter->getRoles() == ["ROLE_USER","ROLE_ENSEIGNANT"])
            {
                $receiver = $demande->getEleve();
            }elseif ($transmitter->getRoles() == ["ROLE_USER","ROLE_ELEVE"]) {
                $receiver = $demande->getEnseignant();
            }else{
                $receiver = null;
            }

            $message = (new Message())
                ->setTransmitter($transmitter)
                ->setReceiver($receiver)
                ->setContent($faker->sentence())
                ->setDate($faker->dateTime('now'))
                ->setDemande($demande)
            ;

            $manager->persist($message);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
            DemandeFixtures::class
        );
    }

}