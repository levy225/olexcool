<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\Matiere;
use App\Entity\Demande;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class DemandeFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create();

        $allusers = $manager->getRepository(User::class)->findAll();
        $eleves = [];
        $enseignants = [];

        foreach ($allusers as $user){
            if(in_array('ROLE_ELEVE', $user->getRoles())){
                $eleves[] = $user;
            }
            elseif(in_array('ROLE_ENSEIGNANT', $user->getRoles())){
                $enseignants[] = $user;
            }
        }

        for($i = 0; $i < 50; $i++){

            $demande = (new Demande())
                ->setDateDemande($faker->dateTimeBetween('-3 months', 'now'))
                ->setCommentaire($faker->sentence('10'))
                ->setTypeDemande($faker->randomElement(Demande::TYPE))
                ->setMatiere($faker->randomElement($manager->getRepository(Matiere::class)->findAll()))
                ->setEleve($faker->randomElement($eleves))
                ->setEnseignant($faker->randomElement($enseignants))
                ->setIsAccepted($faker->randomElement([false, true]))
            ;

            $manager->persist($demande);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
            MatiereFixtures::class,
        );
    }
}
