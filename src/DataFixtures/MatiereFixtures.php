<?php


namespace App\DataFixtures;

use App\Entity\Matiere;
use App\Entity\Niveau;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;


class MatiereFixtures extends Fixture implements DependentFixtureInterface
{

    public function load(ObjectManager $manager)
    {

        $faker = \Faker\Factory::create();

        $niveaux = $manager->getRepository(Niveau::class)->findAll();


        $matiere = (new Matiere())
            ->setLibelle("Maths")
            ->setNiveau($faker->randomElement($niveaux))
            ;
        $manager->persist($matiere);

        $matiere = (new Matiere())
            ->setLibelle("Français")
            ->setNiveau($faker->randomElement($niveaux))
        ;
        $manager->persist($matiere);

        $matiere = (new Matiere())
            ->setLibelle("Histoire")
            ->setNiveau($faker->randomElement($niveaux))
        ;
        $manager->persist($matiere);

        $matiere = (new Matiere())
            ->setLibelle("Physique")
            ->setNiveau($faker->randomElement($niveaux))
        ;
        $manager->persist($matiere);

        $matiere = (new Matiere())
            ->setLibelle("Anglais")
            ->setNiveau($faker->randomElement($niveaux))
        ;
        $manager->persist($matiere);

        $matiere = (new Matiere())
            ->setLibelle("Test")
            ->setNiveau($faker->randomElement($niveaux))
        ;
        $manager->persist($matiere);

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            NiveauFixtures::class,
        );
    }
}