<?php


namespace App\DataFixtures;

use App\Entity\Niveau;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;


class NiveauFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {

        $level = (new Niveau())->setlibelle("Bac +1");
        $manager->persist($level);

        $level = (new Niveau())->setlibelle("Bac +2");
        $manager->persist($level);

        $level = (new Niveau())->setlibelle("Bac +3");
        $manager->persist($level);

        $level = (new Niveau())->setlibelle("Bac +4");
        $manager->persist($level);

        $level = (new Niveau())->setlibelle("Bac +5");
        $manager->persist($level);

        $level = (new Niveau())->setlibelle("Bac +6");
        $manager->persist($level);

        $level = (new Niveau())->setlibelle("Bac +7");
        $manager->persist($level);

        $level = (new Niveau())->setlibelle("Bac +8");
        $manager->persist($level);

        $manager->flush();
    }
}