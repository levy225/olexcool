<?php


namespace App\Service;


use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class MailerService extends AbstractController
{
    /**
     * @param $to
     * @param $subject
     * @param $body_text
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function sendEmail($mailer, $to, $subject, $body_text){
        $email = (new Email())
            ->from('olexcool.contact@gmail.com')
            ->to($to)
            ->subject($subject)
            ->text($body_text);
        $mailer->send($email);
    }

    /**
     * @param $to
     * @param $subject
     * @param $body_text
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function sendMail($mailer, $to, $subject, $mail_template, $user){

        $email = (new TemplatedEmail())
            ->from('olexcool.contact@gmail.com')
            ->to($to)
            ->subject($subject)
            ->htmlTemplate($mail_template)
            ->context([
                'user' => $user,
                'token' => $user->getConfirmationToken(),
            ]);

        $mailer->send($email);
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function generateToken()
    {
        return rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '=');
    }
}