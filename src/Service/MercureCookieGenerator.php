<?php

namespace App\Service;

use App\Entity\User;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha384;
use Lcobucci\JWT\Signer\Rsa\Sha256;

class MercureCookieGenerator {
    /**
     * @var string
     */
    private $secret;

    /**
     * MercureCookieGenerator constructor.
     * @param string $secret
     */
    public function __construct(string $secret) {
        $this->secret = $secret;
    }

    public function generate(User $user) {
        $token = (new Builder())
            ->set('mercure', ['subscribe' => ["http://chat.olexcool/user/{$user->getId()}"]])
            ->sign(new Sha384(), $this->secret)
            ->getToken();
        return "mercureAuthorization={$token}; Path=/.well-known/mercure; HttpOnly;";
    }
}