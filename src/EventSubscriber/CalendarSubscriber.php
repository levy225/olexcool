<?php


namespace App\EventSubscriber;

use App\Entity\User;
use CalendarBundle\CalendarEvents;
use CalendarBundle\Entity\Event;
use CalendarBundle\Event\CalendarEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;


class CalendarSubscriber implements EventSubscriberInterface
{

    private $em;

    public function __construct(EntityManagerInterface $entityManager) {
        $this->em = $entityManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            CalendarEvents::SET_DATA => 'onCalendarSetData',
        ];
    }

    public function onCalendarSetData(CalendarEvent $calendar)
    {
        $filters = $calendar->getFilters();

        $enseigant = $this->em->getRepository(User::class)->find($filters['id']);

        $demandes = $enseigant->getDemandeReceive();

        foreach($demandes as $demande) {
            if($demande->getIsAccepted() == true) {
                if ($demande->getDateCours()) {
                    $calendar->addEvent(new Event(
                        $demande->getCommentaire(),
                        $demande->getDateCours()
                    ));
                }
            }
        }

    }

}