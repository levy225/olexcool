install:
	docker-compose run php composer install
	docker-compose run php bin/console d:s:u --force
	yarn install

start:
	docker-compose up -d
	yarn build
	yarn watch

stop:
	docker-compose down

update:
	docker-compose run php bin/console d:s:u --dump-sql --force

load:
	docker-compose exec php bin/console d:f:l

