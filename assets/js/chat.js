let tx = document.getElementsByTagName('textarea')[0];
tx.addEventListener("input", OnInput, false);

function OnInput() {
    this.style.height = '24px';
    this.style.height = this.scrollHeight + 'px';
    document.getElementsByClassName('chat__footer')[0].style.height = this.scrollHeight + 20 + 'px';
}

let chatBody = document.getElementsByClassName('chat__body')[0];
chatBody.scrollTop = chatBody.scrollHeight;

const url = new URL('http://localhost:3000/.well-known/mercure');
url.searchParams.append('topic', 'http://chat.olexcool/message/');
const eventSource = new EventSource(url, {withCredentials: true});
eventSource.onmessage = e => {
    let div = document.createElement('div');
    div.className = "msg msg--other";
    let msgHeader = document.createElement('div');
    msgHeader.className = "msg--header";
    let spanHeader = document.createElement('span');
    spanHeader.appendChild(document.createTextNode(e.data));
    let msgFooter = document.createElement('div');
    msgFooter.className = "msg--footer";
    let spanFooter = document.createElement('span');
    spanFooter.appendChild(document.createTextNode("Maintenant"));

    msgHeader.appendChild(spanHeader);
    msgFooter.appendChild(spanFooter);
    div.appendChild(msgHeader);
    div.appendChild(msgFooter);
    chatBody.appendChild(div);
    chatBody.scrollTop = chatBody.scrollHeight;
};

var $ = require('jquery');

$('#submitChat').click(function() {
    $('#submitChat').prop('disabled', true);
    if ($('#transmitterId').val().length > 0) {
        $.ajax({
            url : '/chat/new/' + $('#demandeId').val() + '/' + $('#transmitterId').val() + '/' + $('#receiverId').val(), //{demande_id}/{transmitter_id}/{receiver_id}
            type : 'POST',
            data: { 
                'content': $('#message_content').val()
            },
            dataType : 'html',
            success : function(code_html, statut){
                console.log('Success');

                let div = document.createElement('div');
                div.className = "msg msg--me";
                let msgHeader = document.createElement('div');
                msgHeader.className = "msg--header";
                let spanHeader = document.createElement('span');
                spanHeader.appendChild(document.createTextNode($('#message_content').val()));
                let msgFooter = document.createElement('div');
                msgFooter.className = "msg--footer";
                let spanFooter = document.createElement('span');
                spanFooter.appendChild(document.createTextNode("Maintenant"));
            
                msgHeader.appendChild(spanHeader);
                msgFooter.appendChild(spanFooter);
                div.appendChild(msgHeader);
                div.appendChild(msgFooter);
                chatBody.appendChild(div);
                chatBody.scrollTop = chatBody.scrollHeight;

                $('#message_content').val('');
                $('#submitChat').prop('disabled', false);
            },
        
            error : function(resultat, statut, erreur){
                $('#submitChat').prop('disabled', false);
            },
        
            complete : function(resultat, statut){
                $('#submitChat').prop('disabled', false);
            }
        
        });
    }
});