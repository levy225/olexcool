document.getElementsByClassName('panel--switch')[0].addEventListener('click', function() {
    document.getElementsByClassName('panel--switch')[0].classList.toggle("active");
    document.getElementsByClassName('panel__block')[0].classList.toggle("mini");
    document.getElementsByClassName('page')[0].classList.toggle("mini");
    if (document.getElementsByClassName('panel--switch')[0].classList.contains("active")) {
        localStorage.setItem('panelActive', true);
    } else {
        localStorage.setItem('panelActive', false);
    }
});

document.getElementsByClassName('page')[0].style.transitionDuration = "0s";
document.getElementsByClassName('panel__block')[0].style.transitionDuration = "0s";
document.getElementsByClassName('logo--icon')[0].style.transitionDuration = "0s";
document.querySelector('.logo--icon svg').style.transitionDuration = "0s";
document.querySelector('.menu--item i').style.transitionDuration = "0s";
document.querySelector('.menu--item span').style.transitionDuration = "0s";
document.getElementsByClassName('menu--item')[0].style.transitionDuration = "0s";
document.getElementsByClassName('footer')[0].style.transitionDuration = "0s";
if (localStorage.getItem('panelActive') === "true") {
    document.getElementsByClassName('panel--switch')[0].classList.add("active");
    document.getElementsByClassName('panel__block')[0].classList.add("mini");
    document.getElementsByClassName('page')[0].classList.add("mini");
} else {
    document.getElementsByClassName('panel--switch')[0].classList.remove("active");
    document.getElementsByClassName('panel__block')[0].classList.remove("mini");
    document.getElementsByClassName('page')[0].classList.remove("mini");
}
setTimeout(() => {
    document.getElementsByClassName('page')[0].style.transitionDuration = ".5s";
    document.getElementsByClassName('panel__block')[0].style.transitionDuration = ".5s";
    document.getElementsByClassName('logo--icon')[0].style.transitionDuration = ".5s";
    document.querySelector('.logo--icon svg').style.transitionDuration = ".5s";
    document.querySelector('.menu--item i').style.transitionDuration = ".3s";
    document.querySelector('.menu--item span').style.transitionDuration = ".3s";
    document.getElementsByClassName('menu--item')[0].style.transitionDuration = ".5s";
    document.getElementsByClassName('footer')[0].style.transitionDuration = ".5s";
}, 100);


document.addEventListener("DOMContentLoaded", function() {
    let flashes =  document.getElementsByClassName('flashes');
    for (let i = 0; i < flashes.length; i++){
        M.toast({html: flashes[i].value });
    }
});