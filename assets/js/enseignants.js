var $ = require('jquery');

$('.mat').each(function(index, el) {
    el.addEventListener("click", function() {
        $(el).css("font-weight", 700);
        $(el).parent().find('.desc').css("font-weight", 300);
        $(el).parent().parent().find('.description').hide();
        $(el).parent().parent().find('.matieres').show();
    })
});
$('.desc').each(function(index, el) {
    el.addEventListener("click", function() {
        $(el).css("font-weight", 700);
        $(el).parent().find('.mat').css("font-weight", 300);
        $(el).parent().parent().find('.description').show();
        $(el).parent().parent().find('.matieres').hide();
    })
});