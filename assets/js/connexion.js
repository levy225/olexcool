document.getElementById('switch--to--signin').addEventListener('click', function() {
    document.getElementsByClassName('img__block')[0].classList.add('signin');
    document.getElementsByClassName('img__block')[0].classList.remove('signup');
    document.getElementById('signup').classList.add('hide');
    document.getElementById('signin').classList.remove('hide');
});
document.getElementById('switch--to--signup').addEventListener('click', function() {
    document.getElementsByClassName('img__block')[0].classList.add('signup');
    document.getElementsByClassName('img__block')[0].classList.remove('signin');
    document.getElementById('signin').classList.add('hide');
    document.getElementById('signup').classList.remove('hide');
});
document.getElementsByClassName('connexion--btn')[0].addEventListener('click', function() {
    document.getElementsByClassName('img__block')[0].classList.remove('confirm');
});