let demand = document.getElementById('demande');
let date = demand.childNodes[2];

let type = document.getElementById('demande_type_demande');
type.addEventListener("change", () => {
    if (type.value == "c"){
        date.style["display"] = "block";
    } else {
        date.style["display"] = "none";
        document.getElementById('demande_date_cours').removeAttribute('required');
    }
});